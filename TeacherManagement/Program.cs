﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TeacherManagement
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter the number of teachers : ");
            int n = int.Parse(Console.ReadLine());
            List<GiaoVien> senseiList = new List<GiaoVien>();
            try
            {
                senseiList = getSenseiInfo(senseiList, n);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("--------------------------------------------");
            Console.Write("Press any key to continue ...");
            Console.ReadLine();

            Console.Clear();
            Console.WriteLine("Teacher which the lowest salary : ");
            try
            {
                LowestSalaryTeacher(senseiList).XuatThongTin();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.Write("Press any key to exit ...");
            Console.ReadLine();
        }
        public static List<GiaoVien> getSenseiInfo(List<GiaoVien> senseiList, int n)
        {
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("--------------------------------------------");
                Console.WriteLine("Teacher number " + (i + 1));

            name_input:
                Console.Write("Name : ");
                string name = Console.ReadLine();
                if (name == "")
                {
                    Console.WriteLine("Name can't be empty, please enter it again! ");
                    goto name_input;
                }

            yob_input:
                Console.Write("Year of birth : ");
                string yob_in = Console.ReadLine();
                int yob;
                int current_year = DateTime.Now.Year;
                if (!int.TryParse(yob_in, out yob))
                {
                    Console.WriteLine("yob must be a number, please enter it again! ");
                    goto yob_input;
                }
                else
                {
                    if (current_year - yob >= 123)
                    {
                        Console.Write("Are you human ??? (y or n): ");
                        string human_check = Console.ReadLine();
                        if(human_check == "y" || human_check == "yes")
                        {
                            Console.WriteLine("(0_0').... Seriously?!...");
                            goto yob_input;
                        }
                        else if(human_check == "n" || human_check == "no")
                        {
                            Console.WriteLine("...oh...uhm...okay...");
                            Console.WriteLine();
                        }
                    }
                }

            salary_input:
                Console.Write("Salary : ");
                string salary_in = Console.ReadLine();
                double salary;
                if (!double.TryParse(salary_in, out salary))
                {
                    Console.WriteLine("coef must be a number, please enter it again! ");
                    goto salary_input;
                }

            coef_input:
                Console.Write("Coef : ");
                string coef_in = Console.ReadLine();
                float coef;
                if (!float.TryParse(coef_in, out coef))
                {
                    Console.WriteLine("coef must be a number, please enter it again! ");
                    goto coef_input;
                }

                GiaoVien sensei = new GiaoVien();
                sensei.NhapThongTin(name, yob, salary);
                sensei.NhapThongTin(coef);
                senseiList.Add(sensei);
            }
            return senseiList;
        }
        public static GiaoVien LowestSalaryTeacher(List<GiaoVien> senseiList)
        {
            GiaoVien target = senseiList.OrderBy(s => s.TinhLuong()).First();
            return target;
        }
    }
}
