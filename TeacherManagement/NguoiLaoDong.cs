﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeacherManagement
{
    class NguoiLaoDong
    {
        private string HoTen;
        private int NamSinh;
        private double LuongCoBan;
        public string hoten
        {
            get { return HoTen; }
            set { HoTen = value; }
        }
        public int namsinh
        {
            get { return NamSinh; }
            set { NamSinh = value; }
        }
        public double luongcoban
        {
            get { return LuongCoBan; }
            set { LuongCoBan = value; }
        }
        public NguoiLaoDong() {}
        public NguoiLaoDong(string hoten, int namsinh, double luong)
        {
            this.hoten = hoten;
            this.namsinh = namsinh;
            this.luongcoban = luong;
        }
        ~NguoiLaoDong() { }
        public virtual void NhapThongTin(string hoten, int namsinh, double luong)
        {
            this.hoten = hoten;
            this.namsinh = namsinh;
            this.luongcoban = luong;
        }
        public virtual void NhapThongTin(float hesoluong) { }
        public virtual double TinhLuong()
        {
            return this.luongcoban;
        }
        public virtual void XuatThongTin()
        {
            Console.WriteLine("Ho ten la : " + this.hoten + ", nam sinh : " + this.namsinh + ", luong co ban : " + this.luongcoban);
        }
    }
    class GiaoVien : NguoiLaoDong
    {
        private float HeSoLuong;
        public float hesoluong
        {
            get { return HeSoLuong; }
            set { HeSoLuong = value; }
        }
        public GiaoVien() { }
        public GiaoVien(string hoten, int namsinh, double luong, float hesoluong) : base(hoten, namsinh, luong)
        {
            this.hesoluong = hesoluong;
        }
        public override void NhapThongTin(float hesoluong)
        {
            this.hesoluong = hesoluong;
            this.XuLy();
        } 
        public override double TinhLuong()
        {
            return this.luongcoban * hesoluong * 1.25;
        }
        public override void XuatThongTin()
        {
            Console.WriteLine("Ho ten la : " + this.hoten + ", nam sinh : " + this.namsinh + ", luong : " + this.TinhLuong());
        }
        public void XuLy()
        {
            this.hesoluong += (float)0.6;
        }
    }
}
